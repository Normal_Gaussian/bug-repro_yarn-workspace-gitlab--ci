const atob = require('atob')
const btoa = require('btoa')

console.log({
    atob: !!atob,
    btoa: !!btoa,
    usage: btoa(atob("42"))
})
